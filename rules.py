from util import pathstr


class Action(object):

    'An action (command) in a Makefile rule'

    __slots__ = ['command', 'ignore_exit_status']

    def __init__(self, command, ignore_exit_status=False):
        self.command = command
        self.ignore_exit_status = ignore_exit_status

    @property
    def _prefix(self):
        return '\t-' if self.ignore_exit_status else '\t'

    def __str__(self):
        return self._prefix + str(self.command)


def as_action(obj):
    is_action = isinstance(obj, Action)
    return obj if is_action else Action(obj)

def ignore_exit_status(command):
    return Action(command, True)


class Rule(object):

    'A rule in a Makefile'

    __slots__ = ['target', 'inputs', 'actions', 'phony', 'meta']

    def __init__(self, target, inputs, actions, phony=False, meta=dict()):
        self.target = target
        self.inputs = inputs
        self.actions = [as_action(a) for a in actions]
        self.phony = phony
        self.meta = meta

    def _target_str(self):
        return pathstr(self.target)

    def _inputs_str(self):
        return ' '.join(map(pathstr, self.inputs)) if self.inputs else ';'

    def _preheader_lines(self):
        return ['.PHONY: {:s}'.format(self.target)] if self.phony else []

    def _header_lines(self):
        return [' : '.join([self._target_str(), self._inputs_str()])]

    def _actions_lines(self):
        return [(str(action)) for action in self.actions]

    def lines(self):
        return (self._preheader_lines()
                + self._header_lines()
                + self._actions_lines())

    def __str__(self):
        return '\n'.join(self.lines())

    def update_meta(self, **kwargs):
        self.meta.update(kwargs)
        return self


def to_makefile(rules, sep='\n\n'):
    return sep.join(map(str, rules))
