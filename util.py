from __future__ import print_function

import sys
import json


def pathstr(p):
    return str(p)

def wrap_curly(s):
    return '(' + s + ')'

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def to_json(x, sort_keys=False):
    return json.dumps(x, indent=2, sort_keys=sort_keys)

